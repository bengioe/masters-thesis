% Packages (fold)
\RequirePackage{lmodern}
\documentclass[12pt, oneside, extrafontsizes]{memoir}  % TODO 12pt, twoside

\usepackage[utf8]{inputenc}
\usepackage[a-1b]{pdfx}
\usepackage[colorlinks,bookmarksnumbered,bookmarksdepth=subsubsection,unicode=true]{hyperref}


\setstocksize{11in}{8.5in}
\settrimmedsize{11in}{8.5in}{*}
\settrims{0in}{0in}
\setlrmarginsandblock{38mm}{25mm}{*}
\setulmarginsandblock{25mm}{25mm}{*}
\setheadfoot{13pt}{26pt}
\setheaderspaces{*}{13pt}{*}
\checkandfixthelayout
\DoubleSpacing
\setsecnumdepth{subsubsection}
\headstyles{default}
\chapterstyle{ell}
\setsecheadstyle{\scshape\LARGE\raggedright}

\newcommand{\thistitle}{On Reinforcement Learning for Deep Neural Architectures : \\ \large{Conditional computation with stochastic computation policies}}


\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{xfrac}
\usepackage{dsfont}
\usepackage{xspace}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{standalone}
\usepackage[boxed]{algorithm2e}
\usepackage{caption}
\usepackage{subcaption}

% Custom commands
\newcommand{\rvs}{r.v.'s\xspace}
\newcommand{\mdp}{Markov Decision Process\xspace}
\newcommand{\mdps}{Markov Decision Processes\xspace}
\newcommand{\mrp}{Markov Reward Process\xspace}
\newcommand{\mc}{Markov Chain\xspace}


\newcommand{\ann}{artificial neural network}
\newcommand{\Ann}{Artificial neural network}
\newcommand{\rl}{reinforcement learning\xspace}
\newcommand{\Rl}{Reinforcement learning\xspace}
\newcommand{\R}{\mathbb{R}}
\newcommand{\iid}{i.i.d.\xspace}




\def\given{\;\middle\vert\;}
\def\optimal{\star}
\def\transpose{\intercal}
\def\laplacian{\mathbf{\mathcal{L}}}
\def\eqdef{\overset{\underset{\mathrm{def}}{}}{=}}
\def\indicator{\mathds{1}}
\DeclareMathOperator{\expectation}{\mathbb{E}}
\DeclareMathOperator{\vol}{\text{vol}}

\newcommand{\todo}[1]{[TODO: #1]}
\newcommand{\tocite}[1]{[TODO, cite: #1]}
\newcommand{\termidx}[1]{\index{#1}{\textbf{#1}}}
\newcommand{\dd}[2]{\frac{\partial #1}{\partial #2}}

\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary}

\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem{exmp}{Example}[section]

\usepackage{csquotes}
\usepackage{showidx}
\makeindex

%\usepackage{cite}
\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,open={(},close={)}}
%\usepackage[backend=biber, citestyle=authoryear, bibstyle=authoryear, isbn=false, url=false, doi=false, eprint=false, natbib=false, sorting=nty, uniquename=init]{biblatex}
%\addbibresource{library.bib}



\usepackage{titlesec}
\usepackage{etoolbox}

\makeatletter
\patchcmd{\ttlh@hang}{\parindent\z@}{\parindent\z@\leavevmode}{}{}
\patchcmd{\ttlh@hang}{\noindent}{}{}{}
\makeatother

%\titleformat{\section}
%  {\fontsize{18}{18}}{\thesection}{1em}{}



\newsubfloat{figure}  % must follow hyperref
\hypersetup{
%pdfauthor = {Emmanuel Bengio},
%pdftitle = {\thistitle},
%pdfsubject = {Subject},
%pdfkeywords = {reinforcement learning, markov chains, hierarchical reinforcement learning},
%pdfcreator = {LaTeX with the hyperref package},
%pdfproducer = {},
linkcolor = [HTML]{000000},
citecolor = [HTML]{002288},
pdfpagemode=UseOutlines,    % this is the option you were lookin for
%urlcolor = [HTML]{\colorc}
}
\usepackage[all]{hypcap}




\titleformat*{\section}{\Large\scshape}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Title (fold)
\pretitle{\begin{center}\cftchapterfont\huge}
\posttitle{\end{center}}
\preauthor{\begin{center}\huge}
\postauthor{\end{center}}
\predate{\begin{center}\large}
\postdate{\end{center}}

\title{\thistitle}
\author{Emmanuel Bengio}
\date{\today}
\renewcommand\maketitlehookb{
\vfill
}
\renewcommand\maketitlehookc{
\vfill
\begin{center}
{
\large
Computer Science\\
McGill University, Montreal
}
\end{center}
\vspace{10mm}
}
\renewcommand\maketitlehookd{
\vspace{10mm}
A thesis submitted to McGill University in partial fulfilment of the requirements of
the degree of Master of Science.
\copyright Emmanuel Bengio; \today.
}
% Title (end)

\begin{titlingpage}
\maketitle
\end{titlingpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ackowledgements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\clearpage
%\pagenumbering{roman}
%\renewcommand{\abstractname}{Dedication}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ackowledgements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\pagenumbering{roman}
\renewcommand{\abstractname}{Acknowledgements}
\begin{abstract}

  I would like to start by thanking all those who have fostered my creativity throughout my life, and gave me the freedom to explore the world as well as my own mind. It is a gift which I cherish.

  I would also like to thank Joelle Pineau and Doina Precup for guiding me through my master's and keeping me focused on the tasks at hand, and Pierre-Luc Bacon for the initial idea about this whole project as well as for his enlightening insights on reinforcement learning.
  
\end{abstract}

\clearpage %%%%
\renewcommand{\abstractname}{Abstract}
\begin{abstract}
  Deep learning methods have recently started dominating the machine learning world as they offer state-of-the-art performance in many applications. Unfortunately, the learning and evaluation of deep models is time-consuming, sometimes prohibitive, on resource-constrained devices such as phones and mobile hardware. Conditional computation attempts to alleviate this problem by selectively computing only parts of some model at a time.

  This thesis investigates the use of reinforcement learning algorithms as a tool to learn the computation selection strategies inside deep neural network models. Efficient ways of structuring these deep models are proposed, as well as parameterizations for activation-dependent computation policies. A learning scheme is then proposed, motivated by sparsity and computation speed as well as the desire to retain prediction accuracy. By using a regularized policy gradient algorithm, policies are learned which allow stable performance at a lower computational cost. Encouraging empirical results are presented, suggesting that the general framework presented in this work is a sensible basis for the problem of conditional computation.
\end{abstract}

\clearpage %%%%
\renewcommand{\abstractname}{Résumé}
\begin{abstract}
  Les méthodes d'apprentissage profond ont récemment commencé à dominer le monde de l'apprentissage automatique car elles offrent des performances à l'état de l'art dans de nombreuses applications. Malheureusement, l'apprentissage et l'utilisation de ces modèles profonds est très demandant en ressources computationelles, voir même impossible sur des plateformes avec des ressources limitées, tels que les cellulaires et autres appareils mobiles. Le calcul conditionel tente de règler ce problème en sélectionnant activement certaines parties du modèles à calculer.

  Cette thèse explore l'utilisation d'algorithmes d'apprentissage par renforcement comme outil pour apprendre des stratégies de calcul conditionel à l'intérieur de réseaux de neurones profonds. Des manières efficaces de structurer ces modèles profonds sont proposées, ainsi que des paramétrisations pour des politiques de calcul conditionel dépendantes des activations neuronales. Une méthode d'apprentissage est proposée, motivée par le calcul éparse et le temps de calcul, ainsi que le désir de maintenir la précision des prédictions. En utilisant des algorithmes de \emph{policy gradient} régularisés, les politiques de calcul apprises permettent d'obtenir des performances stables à un coût de calcul moindre. Des résultats empiriques encourageants sont présentés, suggérant que les algorithmes présentés dans ce travail sont une base sensible pour le problème du calcul conditionel.
\end{abstract}

%%%%%%% TOC %%%%%%%%
\clearpage
\setcounter{tocdepth}{2}
\tableofcontents
\newpage
\listoffigures
\listofalgorithms

\clearpage
\pagenumbering{arabic}
\chapter{Introduction}
\input{chapters/introduction}

\chapter{Artificial Neural Networks}% and Backpropagation}
\label{chap:ann}
\input{chapters/ann}

\chapter{Reinforcement Learning}
\label{chap:rl}
\input{chapters/rl}

\chapter[Conditional Computation]{Conditional Computation in Deep Networks through Policies}
\label{chap:condcomp}
\input{chapters/condcomp}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Empirical Evaluation}
\label{chap:experiments}
\input{chapters/experiments}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Conclusion and future work}

Sparse computations seem like an inherently hard problem with modern architectures, which perform so well for dense computations. Nonetheless, leveraging Reinforcement Learning methods to reason about the structure of computations allow us to take advantage of the structure of Neural Networks, retaining strong accuracy and still managing to decrease computation time by using sparse algorithms.

Despite the optimization problems % variance and chicken-and-egg
that learning such models poses, using strong regularization terms encouraging sparsity and varied activations enables them to perform just as well as their dense counterparts. The hyperparameters for such regularization can be tuned easily, and nicely correlate with the trade-off between precision and computation time. As such, it is possible to manually control this trade-off by varying these hyperparameters.

Using specialized algorithms that perform sparse computations based on the conditional policies, it is possible to get large speedups both on CPU and GPGPU architectures. Depending on the size of the data, the parameters, the minibatch, and the sparsity rates, sparse methods can slow down computations on one end, while on the other end, large speedups of more than 10 times can be obtained, despited using hardware unfavorable to sparse memory accesses.

There remain many challenges and possible improvements to the presented framework. It is still unclear whether the Reinforcement Learning methods that are used here are optimal. Even though they perform better than a random baseline, alternative methods taking into account the temporality of the sequence of computations that are performed in Neural Networks could perform better. Optimization also remains an issue, with training times being two to three times longer than normal networks. Similarly, the combination of the current Reinforcement Learning technique, REINFORCE, and the block structure of computation that is designed in Neural Networks might not be the most amenable to partial computation. Structures such as trees or recurrent models might offer different but more powerful conditional computation architecures, although at an additional optimization cost. It is also possible that entirely novel architectures of computation could be even more amenable to sparse computations.

There are also many aspects of this framework that need to be understood. There is an interplay during learning between what policy is currently used and what direction the main model parameters tend to. It should be possible to caracterize this interaction, understand whether it is beneficial to the resulting model, or whether it could be improved or accomplished differently.

It would also be interesting to explore how different such models are from normal models, how robust they are to changes in policy, or whether they learn different features. With high sparsity, many less parameters are changed at each learning iteration, and this might affect neural network problems such as the vanishing gradient, possibly amplifying it; it may as well be interesting to see the effect of momentum methods on such optimization, as these methods could compensate for the mostly sparse gradient signals.

Overall, there are good empirical reasons to believe that using Reinforcement Learning to take hard decisions in Neural Networks is a sensible choice. The results presented in this work support that such a choice is a sensible framework for Conditional Computation.


%\input{chapters/conclusion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{library}
%\bibliographystyle{plain}
%\printbibliography
\end{document}

%Action: A send ``message'', reward: delta in cost of the receiver B of the message after seeing the examples that generated the message in A
