from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['text.latex.preamble']=["\\usepackage{lmodern}\n\\usepackage{xfrac}"]
params = {'text.usetex' : True,
          'font.size' : 16,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          }
plt.rcParams.update(params)
from noise._perlin import noise2

fig = plt.figure(figsize=(12,6))
ax = fig.gca(projection='3d')
X = np.arange(-3, 3, 0.02)
#X += np.random.random(X.shape)*2
Y = np.arange(-3, 3, 0.02)
#Y += np.random.random(X.shape)*2
Z = np.float32([[noise2(i,j,4) for i in X] for j in Y])
X, Y = np.meshgrid(X, Y)
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.terrain,
                       linewidth=0, antialiased=False)
ax.set_zlim(-1.01, 1.01)
ax.view_init(elev=45., azim=45)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
print "saving"

#fig.colorbar(surf, shrink=0.5, aspect=5)
plt.savefig("valleys.png")
plt.show()
