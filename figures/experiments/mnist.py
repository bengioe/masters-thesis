from theano_tools import*

data = GenericClassificationDataset("mnist")

import numpy

from scipy.misc import imsave

x = data.train[0][:20].reshape((2,10,28,28))
x = numpy.hstack(x)
x = numpy.hstack(x)
imsave('mnist.png', x)

x = data.train[0][:6*15].reshape((6,15,28,28))
x = numpy.hstack(x)
x = numpy.hstack(x)
imsave('mnist_60.png', x)

exit()
data = GenericClassificationDataset("cifar10")

x = data.train[0][:20].reshape((2,10,3,32,32)).transpose(0,1,3,4,2)
x = numpy.hstack(x)
x = numpy.hstack(x)
imsave('cifar.png', x)


data = GenericClassificationDataset("svhn","/data/svhn_crop/svhn.pkl")


x = data.train[0][:20].reshape((2,10,32,32,3))
x = numpy.hstack(x)
x = numpy.hstack(x)
imsave('svhn.png', x)

