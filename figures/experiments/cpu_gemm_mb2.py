from theano_tools.deep import*
import time

ttimes = []
otimes = []

mbs = 1
n = 32
bs = 32

a = shared('a',(mbs, n*bs), 'uniform')
am = shared('am', (mbs,n))
b = shared('b',(n*bs, n*bs), 'uniform')
om = shared('om', (mbs,n))
c = shared('b',(n*bs,), 'uniform')

o = sparse_dot(a,am,b,om,c,bs)
to = sparse_dot_theano(a,am,b,om,c,bs)

f = theano.function([],[o])
ft = theano.function([], [to])

nct = 100

ttimes = numpy.zeros(nct-1)
otimes = numpy.zeros(nct-1)

for k in range(40):

    for i in range(1,nct):
        am.set_value(numpy.float32(numpy.random.uniform(0,1,(mbs,n)) < i/100.))
        om.set_value(numpy.float32(numpy.random.uniform(0,1,(mbs,n)) < i/100.))
        t0 = time.time()
        for j in range(10*32):
            f()
        t1 = time.time()
        for j in range(10*32):
            ft()
        t2 = time.time()

        ttimes[i-1] += t2-t1
        otimes[i-1] += t1-t0
        print k,i,t2-t1, t1-t0
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as pp
pp.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
params = {'text.usetex' : True,
          'font.size' : 13,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          }
pp.rcParams.update(params)

pp.plot(numpy.arange(1,nct)/100.,ttimes,'go-',label='BLAS gemm')
pp.plot(numpy.arange(1,nct)/100.,otimes,'b^-',label='ours')
pp.gca().set_ylabel("time (s)")
pp.gca().set_xlabel("ratio of activated blocks")
pp.grid()
pp.legend()
pp.savefig('cpu_gemm_mb1.pdf')
