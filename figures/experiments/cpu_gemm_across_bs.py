from theano_tools.deep import*
import time

ttimes = []
otimes = []
blocksizes = [4,8,16,32,64,128]

ttimes = numpy.zeros(len(blocksizes))
otimes = numpy.zeros(len(blocksizes))

for p,bs in enumerate(blocksizes):
    mbs = 32
    for k in range(8):

        n = int(256*(k+1)/bs)
        #bs = 32

        a = shared('a',(mbs, n*bs), 'uniform')
        am = shared('am', (mbs,n))
        b = shared('b',(n*bs, n*bs), 'uniform')
        om = shared('om', (mbs,n))
        c = shared('b',(n*bs,), 'uniform')

        o = sparse_dot(a,am,b,om,c,bs)
        to = sparse_dot_theano(a,am,b,om,c,bs)

        f = theano.function([],[o])
        ft = theano.function([], [to])
        for i in range(1,20):
            am.set_value(numpy.float32(numpy.random.uniform(0,1,(mbs,n)) < i/100.))
            om.set_value(numpy.float32(numpy.random.uniform(0,1,(mbs,n)) < i/100.))
            t0 = time.time()
            for j in range(10):
                f()
            t1 = time.time()
            for j in range(10):
                ft()
            t2 = time.time()

            ttimes[p] += t2-t1
            otimes[p] += t1-t0
            print t2-t1, t1-t0
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as pp
pp.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
params = {'text.usetex' : True,
          'font.size' : 13,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          }
pp.rcParams.update(params)

pp.plot(blocksizes,ttimes,'go-',label='BLAS gemm')
pp.plot(blocksizes,otimes,'b^-',label='ours')
pp.gca().set_ylabel("time (s)")
pp.gca().set_xlabel("size of blocks")
pp.grid()
pp.legend()
pp.savefig('cpu_gemm_accross_bs.pdf')
