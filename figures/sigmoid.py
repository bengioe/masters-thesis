import matplotlib.pyplot as pp
from numpy import*

pp.rcParams['text.latex.preamble']=["\\usepackage{lmodern}\n\\usepackage{xfrac}"]
params = {'text.usetex' : True,
          'font.size' : 16,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          }
pp.rcParams.update(params)

f = lambda x: 1/(1.+exp(-x))

x = arange(1600) / 200. - 4
y = f(x)

pp.plot(x,y,'b-',label='$f(x)=1/(1+e^{-x})$')
pp.legend(loc=2)
pp.savefig('sigmoid.pdf')
pp.show()
