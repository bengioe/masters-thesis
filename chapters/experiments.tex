
In this chapter we cover both implementation details and empirical results of experiments made to illustrate the capabilities of the conditional computation framework. For our experiments we made extensive use of the Theano library \citep{theano2016short}, and implement naive sparse computation algorithms.




\section{Sparse Matrix Operations}
\label{section-sparse-mat-op}
In Section \ref{section-fcarch} we showed how to build a fully-connected block architecture, and in Section \ref{section-guiding-policy-search} we showed how to regularize such architectures so that they are sparse.

If the computation policies are sparse enough, then it becomes advantageous to explicitly \emph{not} compute zero'ed units (which would otherwise be zeroed via a multiplicative mask).

We implemented such an algorithm on both CPU (in C) and GPU (in CUDA), as Theano Ops (building blocks of the library's computation graphs).


In the convolutional case, the model described in Section \ref{section-convarch} has a very simple conditional structure. If a layer's flag is 0, it is simply not computed and its input is passed on to the next layer. As such there is no need for any specialized implementation.

\subsection{Structure of sparse blocks}

Consider two layers, the input layer $i-1$ and output layer $i$, for each of these layers the policy was sampled into a binary vector, which we will refer to as mask. In the minibatch setting, the many vectors are joined into mask matrices $M^{(i-1)}$ and $M^{(i)}$. As such the computation (implicitly taking into account bias and activation function) of the output of layer $i$ is:
\definecolor{darkgreen}{rgb}{0,0.5,0}
\begin{equation}{\color{cyan}H^{(i)}} = ({\color{blue}H^{(i-1)}}\times {\color{darkgreen}W^{(i)}})\odot {\color{red}M^{(i)}}\end{equation}
where in fact $H^{(i-1)}=H^{(i-1)} \odot M^{(i-1)}$. The structure of the non-zero entries in this computation is illustrated in Figure \ref{fig-sparse-matrix}.

\begin{figure}[!h]
  \centering
%\includegraphics[scale=0.6]{figures/}
  \input{figures/sparsedot.tex}
  \caption{A sparse matrix-matrix product}
  \label{fig-sparse-matrix}
\end{figure}

\subsection{CPU implementation}

A very natural algorithm follows from this sparse structure.

For every non-zero entry in $M^{(i)}$, compute the dot product at that location, say $x,y$, only taking into account $W^{(i)}_{xk}$ and $H^{(i-1)}_{ky}$ where $M^{(i-1)}_{ky}$ is non-zero. Since each non-zero entry of the masks spans several units (because they are grouped in blocks), the full masks need not to be represented explicitly, and this computation happens somewhat locally densely (in terms of memory local to that block).

Implementing this algorithm in the C programming language, we can measure the impact of using the algorithm versus doing the full computation (which in Theano is done using BLAS's gemm, GEneral Matrix Multiplication operation).

Figure \ref{fig-cpu-gemm} illustrates the obtained speedup for a particular instance, 32 blocks of size 32, with a minibatch size of 32. We can observe that below 30\% sparsity, using such a sparse dot product implementation is faster. 
\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.6]{figures/experiments/cpu_gemm.pdf}

  \caption{Timing of a sparse matrix-matrix product for different sparsity rates.}
  \label{fig-cpu-gemm}
\end{figure}

We also measure how this speedup evolves with respect to the size of blocks. This is illustrated in Figure \ref{fig-cpu-gemm-blocksize}. We observe that having too small blocks hinders performance. At the same time, it is not necessarily enviable, from the target model's point of view, to have very large blocks, thus a trade-off is necessary.
\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.6]{figures/experiments/cpu_gemm_accross_bs.pdf}

  \caption{Timing of a sparse matrix-matrix product for different block sizes, averaged over different sparsity rates.}
  \label{fig-cpu-gemm-blocksize}
\end{figure}


\subsection{Anatomy of a GPGPU}

General Purpose Graphical Processing Units, GPGPUs, are specialized hardware mainly used to generate rasterized images, but which can also be used to achieve significant computation gains.

GPGPUs are a very advantageous hardware to use when doing parallel computations, because they are inherently massively parallel. Such computations include matrix multiplication, and convolutions.

GPGPUs are made of hundreds, sometimes thousands of cores. They have a fundamental difference with typical CPU coreswhich allows many cores to function at the same time. They are grouped together in clusters, and within each cluster every core \emph{has to be executing the same instruction}. GPGPU programs in which this is not the case will cause the execution to branch. If two cores in the same cluster branch off (e.g. as the result of an \texttt{if}), simultaneous execution is no longer possible: the true branch of the \texttt{if} will be executed, and then the false branch (which can cause massive slowdown of execution if it happens often).

\subsection{GPU implementation}

Simply rewriting the CPU implementation in CUDA (for NVIDIA GPGPUs) would not work, since as explained in the last section, conditioning execution of each block on whether its associated flag is 0 or 1 will catastrophically break parallelism.

As such the GPU implementation is done in 2 passes, where the first pass is determining which blocks are non-zero, and the second pass is actually doing the computations for these non-zero blocks only. As such the ``kernel'' (parallel subprogram which is launched for each core) is only launched for blocks where there is computation, so that no branching happens.

Figure \ref{fig-gpu-gemm} illustrates the obtained speedup for a particular instance on GPGPU, again 32 blocks of size 32, with a minibatch size of 32. We can observe that below 17\% sparsity, using this implementation is faster. 

\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.6]{figures/experiments/gpu_gemm.pdf}

  \caption{Timing of a sparse product for different sparsity rates on the GPU.}
  \label{fig-gpu-gemm}
\end{figure}

Comparing figures \ref{fig-cpu-gemm} and \ref{fig-gpu-gemm}, we see that the GPU implementation is much faster than the CPU implementation, but less amenable to sparsity, as the GPGPU cuBLAS implementation of the matrix product is also much faster.

\section{Image Experiments}

We experiment on 3 datasets, MNIST \citep{lecun1998gradient}, CIFAR-10 \citep{krizhevsky2009learning}, and SVHN \citep{netzer2011reading}. We compare neural networks trained with a conditional computation mechanism to those without, and see whether they can achieve similar accuracy while reducing the time required to perform computations.

\subsection{MNIST}

MNIST \citep{lecun1998gradient} is a simple classification dataset consisting of 28$\times$28 grayscale images of handwritten digits. The task is to indentify the class of a digit among 10 possible classes. It has long been regarded as a standard benchmark in the Deep Learning community, and is now commonly used as a sanity check when creating new architectures.

\begin{figure}[!h]
  \centering
  \includegraphics{figures/experiments/mnist.png}
  \caption{MNIST samples}
\end{figure}

For this task, we hand-pick sensible hyperparameters to conduct sanity checks, and we use a target model with a single hidden layer of 16 blocks of 16 units (256 units total), with a target sparsity rate of $\tau=6.25\%=1/16$, learning rates of $10^{-3}$ for the neural network and $5\times10^{-5}$ for the policy,  $\lambda_v=\lambda_s=200$ and $\lambda_{L2}=0.005$. Under these conditions, a test error of around 2.3\% is achieved. A normal (without any conditional computation policy) neural network with the same number of hidden units achieves a test error of around 1.9\%, while a normal neural network with a similar \emph{amount} of computation (multiply-adds) being made (32 hidden units) achieves a test error of around 2.8\%.

While not beating state-of-the-art, which is not our intent here, we obtain low error and similar results to a normal neural network. Since the computation performed in our model is sparse, one could hope that it achieves this performance with less computation time, yet we consistently observe that models that deal with MNIST are too small to allow our specialized sparse implementation (Section \ref{section-sparse-mat-op}) to make a substantial difference. 

\subsubsection{Learned policies}

What is interesting for MNIST is analyzing the policies of conditional computation that were learned for this model.

\renewcommand*{\thesubfigure}{\alph{subfigure}}

\begin{figure}
        % in iclr the width was 0.32*3 and then .192
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{figures/mnist_probs_0.png}
                \caption{Policy activation for samples of class 0}
                \label{fig:mnist_probs_a}
        \end{subfigure}
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{figures/mnist_probs_1.png}
                \caption{Policy activation for samples of class 1}
                \label{fig:mnist_probs_b}
        \end{subfigure}
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{figures/mnist_probs_all.png}
                \caption{Policy activation for samples of all classes}
                \label{fig:mnist_probs}
        \end{subfigure}
        %\begin{subfigure}[b]{0.13\textwidth}
        %        \vspace{0.5em}
        %        \includegraphics[width=\textwidth]{mnist_Z_2.png}
        %        \vspace{0.025em}
        %        \caption{}
        %        \label{fig:sanity-2}
        %\end{subfigure}\\
         
        %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \caption[MNIST density plots of the policies]{ MNIST, (a,b,c), probability distribution of the policy, each example's probability (y axis) of activating each unit (x axis) is plotted as a transparent red dot. Redder regions represent more examples falling in the probability region. Plot (a) is for class '0', (b) for class '1', (c) for all classes.}\label{fig:mnist-policy}
\end{figure}

Figure \ref{fig:mnist-policy} illustrates the activation of the policy units. Each unit, associated with a block of units in the target model, has a particular index on the $x$ axis. On the $y$ axis the policy probabilities, $\sigma_i$s, are plotted as a transparent dot for each sample.

In Figure \ref{fig:mnist_probs_a}, only examples of class 0 are plotted, showing that only some of the blocks are used by the policy. In Figure \ref{fig:mnist_probs_b}, only examples of class 1 are plotted, again showing that only some of the blocks are used, but they are different blocks than for class 0.
Figure \ref{fig:mnist_probs} shows the plot for all classes, showing that across its input space, across classes, the policy has learned to balance which blocks are being used given an input, always achieving some fair amount of sparsity.

Note that regularization is required for such results, but that $\lambda_v$ seems to mostly help to speed up (sometimes drastically) convergence.

These results show that this framework for conditional computation is capable of learning sensible policies that separate the input space and specialize computation to a limited number of computation subsets.

\subsubsection{Is learning the policy helpful?}

As mentioned in Section \ref{section:dropout}, randomly dropping units in a neural network has proved to be an effective method for learning models. Considering this, we compare using random uniform policies, similarly to the dropout scenario \citep{Hinton2012}, to learning conditional computation policies.

The results can be see in Figure \ref{fig:mnist_error_given_sparsity}, where measured sparsity rates are plotted against validation error. As sparsity is increased, the performance of a uniform policy on MNIST gets worse and worse, while the performance of a learned computation policy stays stable, even in the most extreme cases. 

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{figures/mnist_error_given_sparsity.pdf}
  \caption{Varying sparsity rates, comparing learned policies to uniform policies}
  \label{fig:mnist_error_given_sparsity}
\end{figure}

\subsection{SVHN}

The Street View House Numbers (SVHN, \cite{netzer2011reading}) is a classification dataset consisting of 32$\times$32 RGB images of digits, extracted from images of street building addresses. It is similar to the MNIST dataset, but much harder due to the greater variation in digit orientation and additional texture noise, as well as the confounding nearby digits.

\begin{figure}[!h]
  \centering
  \includegraphics{figures/experiments/svhn.png}
  \caption{Street View House Numbers dataset samples}
\end{figure}

To test the robustness of our framework, we performed a hyperparameter exploration over fully-connected architectures, which is summarized by Figure \ref{fig:svhn_valid_time}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1\textwidth]{figures/svhn_valid_given_time.pdf}
  \caption[SVHN experiment results, comparing our approach to a baseline]{ SVHN, each point is an experiment. The x axis is the time required
    to do a full pass over the valid dataset (log scale, lower is better). NN is a normal neural network, condnet is the present approach. % Note that we plot the full hyperparameter exploration results, which is why condnet results are so varied.
  }
                \label{fig:svhn_valid_time}
\end{figure}

We see that using conditional computation, with a specialized sparse operation, achieves better results than similar fully-connected architectures, at a much lower computational cost. Specific results are selected and shown in Figure \ref{fig:svhnresults}.

It seems that by varying hyperparameters, it is possible to manually control the trade-off between high-accuracy and low computation time. In the next section on CIFAR-10, we design experiments that specifically highlight this fact.

\begin{figure}

  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
\hline 
model & error & $\tau$ & \#blocks & $k$ & test time \tabularnewline
\hline 
\hline 
condnet & .183 & 1/11 & 13,8 & 16 & 1.5s(1.4$\times$)  \tabularnewline
\hline 
condnet & .139 & .04,1/7 & 27,7 & 16 & 2.8s (1.6$\times$)  \tabularnewline
\hline 
condnet & \textbf{.073} & 1/22 & 25,22 & 32 & 10.2s(1.4$\times$)  \tabularnewline
\hline 
NN & .116 & - & 288,928 & 1 & 4.8s \tabularnewline
\hline 
NN & .100  & - & 800,736 & 1 & 10.7s  \tabularnewline
\hline 
NN & .091  & - & 1280,1056 & 1 & 16.8s \tabularnewline
\hline 
\end{tabular}

  \caption[SVHN result details]{SVHN results. condnet: the present approach, NN: Neural Network without the conditional activations. $k$ is the block size. ``test time'' is the time required to do a full pass over the test dataset; in parenthesis is the speedup factor compared to run-time without the specialized implementation.}

  \label{fig:svhnresults}
\end{figure}

\subsection{CIFAR-10}

The CIFAR-10 dataset is a classification dataset of 32$\times$32 natural RGB images of 10 different classes of objects: airplane, automobile, bird, cat, deer, dog, frog, horse, ship, truck.


\begin{figure}[!h]
  \centering
  \includegraphics{figures/experiments/cifar.png}
  \caption{CIFAR-10 dataset samples}
\end{figure}

It is a much harder dataset than SVHN, and it is hard to get less than 50\% test error without using convolutional models. As such we test both fully-connected and convolutional architectures on this dataset.

\subsubsection{The speed-performance trade-off}

Simply by controlling how much sparsity is enforced through the hyperparameter $\lambda_s$ (see Section \ref{section-guiding-policy-search}), it is possible to control the trade-off between having a fast model, and having a low-error model.

This can be seen in Figure \ref{fig:cifar-lambda-s}, where we retrained the same fully-connected condnet architecture with several different values of $\lambda_s$. We plot both the resulting run-time of each model, as well as its validation error, and effectively observe the strong trade-off control effect of $\lambda_s$.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{figures/cifar_valid_given_lambda_large.pdf}
  \caption{The effect of $\lambda_s$ when training fully-connected conditional computation architectures}
  \label{fig:cifar-lambda-s}
\end{figure}

\subsubsection{Regularizing early policies}

By controlling the hyperparameter $\lambda_v$ (see Section \ref{section-guiding-policy-search}), it is possible to encourage policies not to be uniform. This can be beneficial early in training, as it pushes the policies to somehow separate the input space and force the model's computation subsets to specialize.

This can be seen in Figure \ref{fig:cifar-lambda-v}. As $\lambda_v$ gets bigger, the error goes down more rapidly, although it is to be noted that in convergence all the models reach the same error (some faster than others).


\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{figures/cifar_valid_errors_over_time_large.pdf}
  \caption{The effect of $\lambda_v$ when training fully-connected conditional computation architectures}
  \label{fig:cifar-lambda-v}
\end{figure}


\subsubsection{Training convolutional architectures}

We also trained convolutional architectures as shown in Section \ref{section-convarch}. In the convnet case, it is easy architecturally to obtain speedups, but it seems much harder to train policies that correctly separate the input space.

Instead, regardless of complexity of depth, most of the learned policies seemed to divide computation in two, sometimes three groups, and assigned layers to each of those groups, making the policy a much weaker in terms of helping to minimize loss. We show some results in Figure \ref{convresults}.

\begin{figure}
\centering
        \begin{tabular}{|c|c|c|c|c|}
        \hline
        model & test error& \# of layers & $\tau$ &  test time\\
        \hline
        \hline
        %\citet{salimans2016weight} & - & - & 7.21 & 3.5s\\ \hline
        conv-condnet & \textbf{.157}& 12 & 0.5  & 1.03s\\% (1.04$\times$)\\
        conv-condnet & .167 &12 & 0.3&  0.84s \\%(1.3$\times$)\\ % 96 
        conv-condnet & .176 &12 & 0.2&  0.66s \\%(1.6$\times$)\\ % 96 
        conv-condnet & .173 &6 & 0.5& \textbf{0.58s} \\%(1.8$\times$) \\ % 96
        conv-NN & .159 &12 & - & 1.07s \\ % 96 
        \hline
        \end{tabular}
        \caption[CIFAR-10 convolutional model results]{CIFAR-10 convolutional model results. conv-condnet is our approach, conv-NN is a convolutional network with no layers dropped out. Time measured on a NVIDIA Titan X GPU.}
        \label{convresults}
\end{figure}

\section{Empirical Observations on Learning Policies}

\begin{comment}
\subsection{Variance and REINFORCE baselines}

We tried 4 different approaches in order to obtain better policy gradients:
\begin{itemize}
\item using no baseline, $b=0$;
\item using a per example baseline, where $b_x$ is that examples last loss measure $\mathcal{L}_x$;
\item using an exponential moving average baseline, $b_t = (1-\gamma) \mathcal{L}_t + \gamma b_{t-1}$, with $\gamma$ close to but less than 1;
\item learning the $Q$ function directly, as in \emph{policy gradient} \citep{sutton1999policy}.
\end{itemize}

The most stable method to learn policies ended up being the exponential moving average baseline. Learning a bad policy early on can have some devastating effects on the rest of the training. Using no baseline at all made learning the policy very unstable, while learning the $Q$ function gave more uniform policies; we speculate that it is because at the beginning all examples tend to have unpredictable losses and the value learned is more or less constant. On the other hand, a moving average adapts more quickly to the average reward and probably guides REINFORCE better.
\end{comment}

\subsection{Time, computation and energy consumption}

Throughout experiments, wall time of computing the forward pass over the entire validation set in minibatches is the primary measure of how well sparse computations are doing. Time is not necessarily the best measure, but is for now the one that best depicts reality in terms of deployment of Machine Learning models.


%\subsubsection{Counting float operations}

Counting the theoretical number of operations is the simplest comparison measure.
In fact, it is so simple that it is not representative of modern hardware, and how it operates. %We can nonetheless 

In the fully connected case, for two successive sparse layers of $n$ units with a sparsity rate of $\tau$, the complexity of the forward pass is in $O(n^3\tau^2)$.

In the layer-skipping case, it is simply linear in $\tau$, the less layers are active, the less computation happens.


\subsection{Streaming data in the fully-connected case}

A use case where fully-connected conditional computation as presented in this work (and sparse computation implementations) could provide a massive advantage is in the case of streaming data; when each example is passed through the model one at a time, instead of in minibatches of many examples. This is a typical setting in deployment of machine learning models, for example on a mobile phone, where only one example at a time is to be treated.

In the streaming case, the advantages provided by dense computations in neural networks is lost since most matrix-matrix operations become matrix-vector operations, which cannot obtain the same gains.

Repeating measurements for figures \ref{fig-cpu-gemm} and \ref{fig:svhn_valid_time} gives a very different impression in the case of streaming data. Figure \ref{fig-cpu-gemm-mb1} shows the same measurement as \ref{fig-cpu-gemm}-- Figure \ref{fig-stream-svhn} shows the same as \ref{fig:svhn_valid_time} -- with a minibatch of size 1, i.e. the streaming case.

We see in the first case that the threshold at which sparse computations are advantageous is now much higher, more than 70\%, compared to 30\% in the minibatch case. In the second case, we see that almost all models beat their dense fully-connected counterparts, in terms of time against accuracy. Some well performing models are about 10 times faster than fully-connected models with the same accuracy.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{figures/experiments/cpu_gemm_mb1.pdf}

  \caption{Timing of a sparse matrix-matrix product for different sparsity rates and a minibatch size of 1.}
  \label{fig-cpu-gemm-mb1}
\end{figure}


\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{figures/svhn_valid_given_time_streaming.pdf}

  \caption[SVHN results, with minibatch size of 1]{SVHN, each point is an experiment. The x axis is the time required
    to do a full pass over the valid dataset in a streaming setting (log scale, lower is better).}
  \label{fig-stream-svhn}
\end{figure}


\subsection{Measuring power consumption}

Power consumption is a different metric than time, achieving a computation slowly but without as much power as a faster alternative can prove beneficial to any battery-connected computing hardware.

We measured power consumption on a portable x86-64 computer, specifically on a i5-2467M CPU chip, and found that it unfortunately correlates linearly with time. A more interesting hardware to run such an experiment would be ARM CPUs, or even in the extreme case FPGAs (although the bottleneck in this case is memory transfer rather than computation).

\section{Discussion}

Overall our results show that using Reinforcement Learning for conditional computation is a sensible choice. Given the right parameterizations, it is possible to learn policies that separate the input space and specialize subsets of the model parameters. Given the right sparsity regularizations, it is possible to run these models much faster than their dense counterparts.

New hyperparameters add complexity to the algorithm, but each has a clear interpretation, and their effect can easily be monitored by visualizing policies during training (such as in Figure \ref{fig:mnist-policy}). Although sensible to these hyperparameters, the optimization remains relatively robust and it is possible, with little additional effort, to get as good (or sometimes better) results than with plain deep neural networks.


