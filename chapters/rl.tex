

\section{The Reinforcement Learning Setting}

Reinforcement Learning (RL) is concerned about decision making, both in immediate and temporal settings. Note that the material presented in this section is but a small subset of RL methods, necessary to understand the rest of the work. Thorough explanations of the material are found in the works of \cite{SuttonBarto1998} and \cite{Csaba2010}.

RL is commonly viewed as reducing problems to the agent-environment setting, illustrated in Figure \ref{fig:rl-env}. An \textbf{agent}, which can for example be a collection of sensors and actuators or a game playing program, resides in an \textbf{environment}. At each discrete time-step $t$, the agent receives information about its state. From that information, it makes an action, which will (possibly) influence the environment, resulting in a new state observation, along with a reward.

\begin{figure}[!h]
  \centering
\input{figures/rl-environment}
\caption{The RL Environment diagram}
  \label{fig:rl-env}
\end{figure}

Often, the goal will be for the agent to learn to maximize its reward. To do so it may have to overcome several challenges, such as delayed rewards, partially observable states, or vast state and action spaces.

\section{The Markov Decision Process Setting}

Markov decision processes \citep{bellman1956dynamic} are a formal tool used to describe problems in reinforcement learning, as they are well suited to describe total-information stochastic (or deterministic) environments.

A Markov decision process (MDP) $M$ is defined as a tuple $(\mathcal{S}, \mathcal{A}, \mathcal{P}, \mathcal{R})$:
\begin{itemize}
  \item $\mathcal{S}$ is the (possibly infinite) set of states;
  \item $\mathcal{A}$ is the (possibly infinite) set of actions; $\mathcal{A}$ can also be state-dependent;
  \item $\mathcal{P}(s,s',a)$ is the transition probability from $s$ to $s'$ given that action $a$ is taken;
  \item $\mathcal{R}(s,s',a)$ is the reward received from action $a$ when going from $s$ to $s'$.
\end{itemize}
Markovian processes are interesting because they follow this rule:
$$P(s_t|s_{t-1},a_{t-1}) = P(s_t|s_{t-1},a_{t-1},s_{t-2},...,s_0,a_0)$$
which is that the next state only depends on the current state. An agent living in a Markovian environment only needs to know about the most recent state in order to know about the future. It does not require memory (at least in principle). 
In this setting, a sequence of state-action-reward tuples is described as a \emph{trajectory} $\tau$.


\subsection{Policies and Stochastic Policies}

Consider a memory-less agent which at each moment in time receives the current state information, and gets to choose an action. This behaviour can be formalized as a \textbf{policy} $\pi:\mathcal{S}\to\mathcal{A}$, which maps states to actions, indicating what to do at each moment. In a more general way, we can formulate a \textbf{stochastic policy} as a conditional probability distribution $\pi(a|s)$; given a state there is a distribution over each action. We will assume that policies are stochastic for the rest of this work.

It is often the goal of reinforcement learning methods to find a policy which is optimal with respect to the environment that is given, or to find a policy which matches the policy of an expert agent (e.g., a human). A policy which is as good as or better in expectation than all other policies, in terms of accumulating reward, is called an \emph{optimal} policy.

In a stochastic --or simply unexplored-- environment it can be desirable to have a stochastic policy, that is, to take actions according to some state-dependent probability distribution $\pi(a|s)$. A simple case of such a policy is a Bernoulli policy, where one of two actions is chosen at random according to some probability $p$. A slightly better policy might be to make this probability input-dependent, and compute it as a function of the state. Stochastic policies make deriving some learning methods harder, and others simpler, simply due to the nature and properties of random variables.

\subsection{Value Functions}

It is an interesting problem, and often a necessary step, to be able to judge \emph{how good} a given state is. The \textbf{value} of a state is defined recursively as:
\begin{align*}
  V^\pi(s)&=\mathbb{E}_{s'a}\{R(s)|\pi\}\\
  &=\sum_a \pi(a|s) \sum_{s'} \mathcal{P}(s,s',a) \left[\mathcal{R}(s,s',a) + V^\pi(s')\right]
\end{align*}

It is the expected cumulative reward (also known as \emph{return}) that can be received in $s$ and in all following states when following the policy $\pi$.

It is also possible to measure \emph{discounted reward}, and instead of seeing infinitely far in the future in terms of reward, prioritizing nearer states by an exponential factor $\gamma$:
$$V^\pi(s) = \sum_a \pi(a|s) \sum_{s'} \mathcal{P}(s,s',a) \left[\mathcal{R}(s,s',a) + \gamma V^\pi(s')\right]$$

One can also express the \textbf{action-value} function, the expected cumulative reward when in state $s$ and taking action $a$

\begin{equation}
  Q^\pi(s,a) = \sum_{s'} \mathcal{P}(s,s',a) \left[\mathcal{R}(s,s',a) + \gamma V^\pi(s')\right] \label{Q_pi}
\end{equation}

It is desirable to learn or measure the value function of an MDP, as it can allow an agent to chose which state to visit next if it wants to maximize reward. Unfortunately, in complex environments this can be a hard, or even intractable problem.

Under the optimal policy, the value functions are called \emph{optimal} value functions, $V^*$ and $Q^*$, since they represent choosing the optimal action at each time-step.
$$Q^*(s,a) = \sum_{s'} \mathcal{P}(s,s',a) \left[\mathcal{R}(s,s',a) + \gamma V^*(s')\right]$$
$$V^*(s) = \max_a Q^*(s,a)$$

%\subsection{The $\epsilon$-greedy policy}

The simplest policy that can be used in conjunction with a value function is the \textbf{$\epsilon$-greedy policy}, which simply consists in taking the action which leads to the state with highest value function, but take a uniformly random action with probability $\epsilon$.

In the case where $\epsilon = 0$, it is simply the greedy policy.


\section{Learning Value Functions}

There are many algorithms that can learn value functions \citep{SuttonBarto1998}, most of which we will not cover. As described in Section \ref{subsec:function-approx}, it is possible to learn a tabular value function if the number of states (and possibly the number of actions) is small enough, or is discretizable into a small set.

In the other case, if the number of states is continuous or too large, it is also possible to use function approximation to learn $V(s)$ or $Q(s,a)$. Doing so poses a major problem: when going through an MDP we can collect \emph{trajectories} -- sequences of states, actions and rewards -- as a data set, but while one can consider trajectories to be i.i.d., the tuples $(s,a,r,s')$ of which they are made \emph{are not i.i.d.}, which makes them unsuitable for typical function approximation learning methods.

Fortunately, there are ways to mitigate this problem, such as using experience replay \citep{lin1992self}, which makes it possible to learn function approximators on large input spaces \citep{mnih2015human}.

%\subsection{Q-Learning}

The Q-Learning \citep{watkins1989learning} algorithm is an iterative way of learning $Q$ functions. An intuitive derivation of the algorithm follows.

Consider having a parameterized estimate of the $Q$ function $Q_\theta(s,a)$, and consider that we are under the $\epsilon$-greedy policy. We can now rewrite \eqref{Q_pi} as:
\begin{align*}
  Q^\pi(s,a) =& \sum_{s'} \mathcal{P}(s,s',a) \left[\mathcal{R}(s,s',a) + \gamma V^\pi(s')\right] \;\;\;(3.1)\\
  Q_\theta(s,a) =& \sum_{s'} \mathcal{P}(s,s',a) \mathcal{R}(s,s',a) + \sum_{s'} \mathcal{P}(s,s',a) \gamma V^\pi(s')\\
  & \mbox{under a greedy policy $\pi$, }V^\pi_\theta(s') = \max_a Q_\theta(s',a)\\
   =& \mathcal{R}(s,a) + \gamma \sum_{s'} \mathcal{P}(s,s',a)  \max_a Q_\theta(s',a)
\end{align*}

Now consider that we are constantly sampling trajectories from the environment. The transition probability $\mathcal{P}(s,s',a)$ is ``naturally'' taken into account from the sampling. For a given sample $(s_t, a_t, r_t, s_{t+1})$, where $r_t \sim \mathcal{R}(s_t,a_t)$, the following should hold true (assuming $\theta$ is reasonably good):
\begin{equation*}
  Q_\theta(s_t,a_t) \approx r_t + \gamma \max_a Q_\theta(s_{t+1},a)\\
\end{equation*}

Since at the beginning of training $Q_\theta$ will be mostly wrong, this will in fact lead to an error term:
$$\delta = |r_t + \gamma \max_a Q_\theta(s_{t+1},a) - Q_\theta(s_t,a_t)|$$
To reduce the error, one can update $Q_\theta$ to explicitly minimize $\delta$ using the previously seen supervised learning methods\footnote{This is not as simple as it may look, mainly due to the data distribution used in learning \textbf{not} being i.i.d., and due to the bootstrapping problem. There are various methods to make this work, see \cite{mnih2015human} for example.}, or in the tabular case, update $Q(s,a)$:
$$Q^{k+1}(s_t,a_t) \leftarrow Q^k(s_t,a_t) + \alpha [r_t + \max_a Q^k(s_{t+1},a) - Q^k(s_t,a_t)]$$
where $\alpha$ is the learning rate, at which the values of $Q$ are updated.

%bertsekas, martin riedmiller


\section[Learning Policies with Policy Gradient Methods]{Learning Policies with Policy \\ Gradient Methods}

In some systems, it can be advantageous to learn policies \emph{directly}, instead of first learning value functions and selecting optimal actions. Just as for value functions, if the state and action spaces are discrete (or discretizable) and small enough, it is possible to learn an explicit mapping $\mathcal{S}\to\mathcal{A}$ or as a stochastic mapping $\pi(a|s)$.

In many cases, most commonly when the state space is too large, it is necessary to use function approximation. For this, it is necessary to have a parameterization of the policies either as a deterministic $f_\theta:\mathcal{S}\to\mathcal{A}$ or as a stochastic function $\pi_\theta(a|s)$. 

Learning policies directly has a few advantages, notably when the value functions are hard to approximate or when a stochastic policy is desired. In some cases, when the end-goal is to learn an optimal policy, it can simply be more convenient, and reduce the number of parameters that one has to learn.

Just as it is possible to learn function approximators in the supervised case with gradient descent, similar methods can be applied to parameterized policies, albeit with a few major differences. I the problems that are relevant here, actions are typically ``hard'' decisions, and there does not exist a derivative of some loss (or reward) with respect to having taken the action (or not). As such, it is not trivial to obtain the gradient of the parameters that goes in the direction of maximizing the rewards. Fortunately, there are methods that compute approximations of these gradients. Using these methods to search among the space of policy functions is known as \textbf{policy search}.


\subsection{REINFORCE}
\label{section-reinforce}

REINFORCE \citep{Williams1992}, also known as the likelihood ratio method \citep{glynn1987likelilood}, is a policy search algorithm. It computes an unbiased estimate of the gradient, and uses it to perform policy search in the direction that minimizes loss.

The objective function of a parameterized policy $\pi_\theta$ for the cumulative return (the total reward received) of a trajectory $\tau$ of length $T$ is described as:
\begin{equation*} J(\theta) = \mathbb{E}_\tau^{\pi_\theta}\left\{ \sum_{t=1}^T r(S_t,A_t) \middle| S_0=s_0\right\}
\end{equation*}
where $s_0$ is the initial state of the trajectory. Let $R(\tau)$ denote the return for trajectory $\tau$. The gradient of the objective with respect to the parameters of the policy is:
\begin{align}
  \nabla_\theta J(\theta) &=\nabla_\theta \mathbb{E}_\tau^{\pi_\theta} \{R(\tau)\} \notag \\
  &= \nabla_\theta \int_\tau \mathbb{P}\{\tau|\theta\}R(\tau)d\tau \notag\\
  &= \int_\tau \nabla_\theta \left[\mathbb{P}\{\tau|\theta\} R(\tau)\right] d\tau \label{exchange}
\end{align}
Note that the interchange in \eqref{exchange} is only valid under some assumptions (see \cite{Silver2014}).
\begin{align}
  \nabla_\theta J(\theta) &=  \int_\tau \nabla_\theta
     \left[\mathbb{P}\{\tau|\theta\} R(\tau)\right] d\tau \notag \\
  &= \int_\tau \left[R(\tau)\nabla_\theta \mathbb{P}\{\tau|\theta\} + \nabla_\theta R(\tau)\mathbb{P}\{\tau|\theta\}\right] d\tau \label{productrule}\\
  &= \int_\tau \left[\frac{R(\tau)}{\mathbb{P}\{\tau|\theta\}} \nabla_\theta \mathbb{P}\{\tau|\theta\} + \nabla_\theta R(\tau)\right] \mathbb{P}\{\tau|\theta\} d\tau \notag \\    
  &= \mathbb{E}_\tau^{\pi_\theta}\left\{R(\tau)\nabla_\theta \log\mathbb{P}\{\tau|\theta\}+\nabla_\theta R(\tau)\right\} \label{logrule}
\end{align}
The product rule of derivatives is used in \eqref{productrule}, and the derivative of a $\log$ in \eqref{logrule}. Since $R(\tau)$ does not depend on $\theta$ directly, the gradient $\nabla_\theta R(\tau)$ is zero. We end up with this gradient:
\begin{align}
\nabla_\theta J(\theta) &= \mathbb{E}_\tau^{\pi_\theta}\left\{R(\tau)\nabla_\theta \log\mathbb{P}\{\tau|\theta\}\right\}
\end{align}
Without knowing the transition probabilities, we cannot compute the probability of our trajectories $\mathbb{P}\{\tau|\theta\}$, or their gradient. Fortunately, we are in a MDP setting, and we can make use of the Markov property of the trajectories to compute the gradient:
\begin{align}
  \nabla_\theta \log\mathbb{P}\{\tau|\theta\} &=\nabla_\theta \log \left[ p(s_0)\prod_{t=1}^T\mathbb{P}\{s_{t+1}|s_t,a_t\}\pi_\theta(a_t|s_t)\right] \notag\\
  &= \nabla_\theta \log p(s_0) + \sum_{t=1}^T \nabla_\theta \log \mathbb{P}\{s_{t+1}|s_t,a_t\} + \nabla_\theta \log \pi_\theta(a_t|s_t) \label{eq:gradiszero}\\
  &= \sum_{t=1}^T\nabla_\theta \log\pi_\theta(a_t|s_t) \notag
\end{align}
In \eqref{eq:gradiszero}, $p(s_0)$ does not depend on $\theta$, so the gradient is zero. Similarly, $\mathbb{P}\{s_{t+1}|s_t,a_t\}$ does not depend on $\theta$ (not in a differentiable way at least), so the gradient is also zero.
We end up with the gradient of the log policy, which is easy to compute.

In the case where the trajectories only have a single step, %and the reward of the trajectory is the neural network cost $C(\mathbf{x})$, thus
the summation disappears and the gradient %found in \eqref{eq:grad-loss-sigm}
is found by taking the log of the probability of the sampled action:
\begin{align}
  \nabla_{\theta} R(s) &= \mathbb{E}\left\{R(s) \nabla_{\theta} \log \pi_{\theta} (a|s) \right\} \label{reinforce-no-baseline}
\end{align} 

\subsubsection{Reducing variance in REINFORCE}

Just as in stochastic gradient descent, it is only useful (and computationally tractable) to use stochastic samples of the data in order to approximate the full gradient in \eqref{reinforce-no-baseline}.

A problem can arise if the length of the trajectories is large or if the number of possible actions per-step is large. Since a particular trajectory or action sample has an intrinsic low probability (simply because there are many possibilities) its gradient's distance to the full expected gradient will be large. This gives rise to a high variance estimator.

There are many ways of reducing variance in policy gradient methods, some work by incurring bias \citep{sutton1999policy}, while some manage to reduce the variance without adding bias \citep{greensmith2004variance, weaver2001optimal}.

A simple way to reduce variance without adding bias in REINFORCE is by modifying the reward signal by subtracting a \emph{baseline} $b$.

\begin{align}
  \nabla_{\theta} R(s) &= \mathbb{E}\left\{(R(s)-b) \nabla_{\theta} \log \pi_{\theta} (a|s) \right\} \label{reinforce-baseline}
\end{align} 

There are many ways to formulate the baseline, the simplest being to use the average reward. In this case an interesting interpretation of REINFORCE can be made, where if an action has a higher reward than the average, its probability will be increased, while if it is lower, its probability will be reduced.
