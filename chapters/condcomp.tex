Conditional computation \citep{bengio2013estimating, davis2013low} is concerned with reducing the amount of computation made by some models, it does so by actively selecting parts of it that are to be computed given the current input. This problem can be viewed as a sequential decision making problem that takes place inside inside a traditional machine learning algorithm.

Having presented artificial neural networks and reinforcement learning, this chapter explains how the two are used in conjunction in order to create efficient conditional computation models.

State-of-the-art models are geting larger and larger every year, and even with the massive increase in computation via distributed systems and massively parallel hardware, it is non-trivial to \emph{deploy} such models such that they can be used in real-time, since most target hardware does not have access to the same kind of massive computation hardware (e.g. mobile phones). As such, there is a need for a computation scheme that speeds up such models. The proposed approach to conditional computation is a step in this direction.

\section{Defining Conditional Computation}

The problem of conditional computation can be expressed as follows: \emph{how can a model choose to perform as few computations as possible, and still make correct predictions?} Let us start by giving a definition of computation.

In a function approximator, the prediction $y=f(x;\theta)$ is typically done using many steps of computation, computing intermediate values up to the final result.
Sometimes these steps can be done in parallel, sometimes one step depends on a previous one.


Let us define a computation graph $\langle V,E\rangle$. We denote the set of intermediate values as $V = \{v_0,v_1,...,v_n\} \cup \{x_0,...,x_d\}$, and the set of dependencies as directed edges $E = \{(v_i,v_j) \;|\; \mbox{computing } v_j \mbox{ requires } v_i\}$.
Conditional computation can act on the edges of this computation graph. Consider the function $a:\mathcal{X}\to \{0,1\}^{|E|}$. Given an input $x$, c outputs a binary vector, associating each edge $(v_a, v_b)$ to either 0 or 1. On a 1, the normal computation occurs. On a 0, $v_b$ is computed while ignoring the value of $v_a$ and considering it to be either 0 or some other predefined value (either a constant, or a ancestor node in the graph). If for all edges $(v_a, v_j)$, the associated $a(x)_{v_a,v_j}$ is 0, then $v_a$ does not need to be computed.

This last phenomenon can be encoded in the function $a:\mathcal{X} \to \{0,1\}^{|V|}$. Given an input $x$, $a(x)_{v_i}=0$ means that $v_i$ does not need to be computed.
Now consider dividing $V$ into disjoint subsets $\nu_i$. Instead of associating to each intermediate value a conditional computation flag $a(x)_{v_i}$, we can associate to each subset a single flag $a(x)_{\nu_i}$.

The success of conditional computation relies on the idea of sparse (and thus faster) computation. If the number of subsets is substantially smaller than the size of $V$, then the computation of $a(x)_\nu$ will only incur a slight overhead. Additionally, if the number of non-zero $a(x)_i$s is small, then most intermediate values will not need to be computed, \emph{resulting in fewer computations being needed}.


%\subsection{Natural groupings of computation in neural networks}

\newenvironment{reusefigure}[2][htbp]
  {\addtocounter{figure}{-1}%
   \renewcommand{\theHfigure}{dupe-fig}% If you're using hyperref
   \renewcommand{\thefigure}{\ref{#2}}% Figure counter is \ref
   \renewcommand{\addcontentsline}[3]{}% Avoid placing figure in LoF
   \begin{figure}[#1]}
  {\end{figure}}
  
\begin{reusefigure}[!h]{fig:mlp}
\centering
%\includegraphics[scale=1]{figures/perceptron}
\input{figures/mlp.tex}
\caption[A multilayer perceptron with 2 hidden layers]{A multilayer perceptron with 2 hidden layers, one of 6 units, one of 3 units. The output layer has 2 units.}
\end{reusefigure}

In neural network models a very natural computation graph arises from the structure of the network.
Consider Figure \ref{fig:mlp} (reproduced here), at layer $i$ each node $h^{(i)}_j$ is computed as:
$$h^{(i)}_j = \sum_k W^{(i)}_{kj}h^{(i-1)}_k$$

There are at least two ``computation-skipping'' mechanisms that we can use in such a computation when the computation flag is 0: akin to dropout \citep{Hinton2012}, ``drop'' the unit $h^{(i)}_j=0$; or, akin to residual networks \citep{he2015deep}, ``skip'' the unit $h^{(i)}_j=h^{(i-1)}_j$.

In the same spirit, there are two natural grouping mechanisms which one can use.
Since many units are often required to disentangle features, it is logical that within a layer some features will often activate together, as observed by \cite{li2015convergent}. As such we can divide hidden layers into groups of hidden units, that are all dropped or activated altogether.

One can also group a whole layer into a computation subset, and conditionally skip layers. This especially makes sense in the computer vision setting, where applying a specific convolutional layer in a very deep network extracts a particular set of visual patterns which might not be present, in which case skipping the layer is justified.

\section{Conditional Computation as a Policy}

This work is an attempt to illustrate that conditional computation in a deep neural network model can naturally be viewed as sequential decision making problem. At each layer, given information on the current state of the computation (what has been computed so far), one has to decide which units to compute.

There is also a natural, delayed reward measure, given by the error at the output of the network; one can further impose rewards that encourage sparsity in the computation, or a fast computation time. This view makes this problem a natural fit for reinforcement learning algorithms.

In the previous section we defined a computation function $a:\mathcal{X}\to\{0,1\}^n$. This function can naturally be seen as a policy $\pi_\theta(a|x)$, where the state space is $\mathcal{X}$ and the action space is this computation flag vector space $\{0,1\}^n$.%, which is simply a vector of Bernoulli units.
The sequential case also takes the same form, where at each layer, decisions about the next layer's computations are made depending on the state of the computation, thus creating a temporal delayed-reward setting with a policy that maps some intermediate computations $\mathcal{H}$ to a binary vector $\{0,1\}^k$.

Finally, in the context of \emph{learning} a conditional computation policy, it is undesirable to have a deterministic policy. For two close deterministic policies, the expected gradient would change greatly between one and the other (since we are always taking the same action for some input), making learning unstable. On the other hand, for two close stochastic policies, the probabilities of actions will be close to one another, and so their \emph{expected} gradients will be similar, making learning more stable.
As such, we will learn and use stochastic Bernoulli policies to achieve conditional computation.

\paragraph{Learning Conditional Computation policies}

$\;$

Now that the problem of conditional computation is framed as a reinforcement learning problem with stochastic policies, this work shows it is possible to learn these models and their policies using the frameworks seen in sections \ref{section-learningdnns} and \ref{section-reinforce}. Note that we will refer to the principal neural network (e.g. the classifier or regressor) as \emph{target}, or target model, and neural network outputting the policy as the \emph{policy model}, or simply \emph{policy}.

In this particular case, the negative reward signal of the policy received after taking actions can be set to be the loss of the target model $\mathcal{L}$, as for example in Section \ref{section-linearmodels}. As such we perform gradient descent on the policy parameters using the REINFORCE estimator in the following form:
\begin{align}
  \nabla_{\theta} \mathcal{L}(x) &= \mathbb{E}_{x\sim X}\left\{(\mathcal{L}(x)-b) \nabla_{\theta} \log \pi_{\theta} (a|x) \right\} \notag
\end{align} 

Here $\pi_\theta(a|x)$ is the probability of the sampled $a$s. We assume there is some parameterization of the policy model $f_\pi(x;\theta)$, possibly a neural network itself, that outputs a vector in $[0,1]^n$, and $a$ is sampled as a vector of Bernoullis with probabilities  $f_\pi(x;\theta) = [\sigma_1\; \sigma_2\; ... \;\sigma_n]^T$. Its log probability is simply:
$$\log\prod_i \left[a_i\sigma_i+(1-a_i)(1-\sigma_i)\right] = \sum_i \log \left[ a_i\sigma_i+(1-a_i)(1-\sigma_i) \right] $$

Note that the learning of the target model must occur simultaneously to the learning of the policy: consider the scenario where blocks of hidden units are dropped (the same reasoning applies in other scenarios). If the target model is learned first, then the units that activate together must be grouped correctly\footnote{As is suggested by litterature concerning dropout, units often coadapt to form some particular computational structure. Even with dropout training, this still occurs, only much less. As such, grouping units arbitrarily post-training has a high chance of perturbing these structures and negatively affecting accuracy.}, otherwise the computation policy will not be of any help; this creates a new, possibly harder, problem of grouping units correctly. On the other hand if the policy is learned first (with the target model at its initial random weights), then the policy has no way of effectively reducing the error, since the target model outputs mostly random and unstructured values (as it has yet to learn).


\section{Guiding Policy Search}
\label{section-guiding-policy-search}

As mentioned in the previous section:
\begin{itemize}
  \item the aim of conditional computation is to perform few computations;
  \item it is easy to impose additional constraints to minimize computations.
\end{itemize}

Such constraints can take two forms. The first, most straightforward one, is to modify the reward signal to include some computation cost, either as a proxy (i.e. the number of activated nodes) or as a direct measure (i.e. elapsed time).
Unfortunately, such a direct modification could add variance to an already high-variance estimator.
The second possible modification is to use regularization. Regularization has the advantage of being differentiable, and as such smoother (it varies consistently with the gradient).

This work introduces three differentiable regularization terms that act on the Bernoulli probabilities $[\sigma_1\; \sigma_2\; ... \;\sigma_n]^T$ of the stochastic policies, rather than on the action samples.
The first two terms, $L_b$ and $L_e$ force the policies to achieve some sparsity hyperparameter target ratio $\tau$.
\begin{align*}
  L_b &= \sum_j^{n}\|\mathbb{E}_{X}\{\sigma_{j}\} - \tau\|^2_2 \\ 
  L_e &= \mathbb{E}_{X}\{\|(\frac{1}{n}\sum_j^{n} \sigma_{j}) - \tau\|^2_2\} 
\end{align*}

The first term $L_b$ penalizes each $\sigma_j$ independently, pushing $\sigma_j$ to be $\tau$ in expectation over the data. The second term $L_e$ penalizes $\sigma_j$s together, forcing that for a given example only some $\sigma_j$s can be high while others are low.

The third term is optional, but speeds up learning, as it forces policies to be ``varied'' and thus to be more input dependent:
\begin{align*}
  L_v &= -\sum_j^n \mbox{var}_i\{\sigma_{ij}\}
\end{align*}

These regularization terms can be optimized via gradient descent, simultaneously to the training of the target model and the policy. The additional loss term can be expressed as:
$$\mathcal{L}_{reg} = \lambda_s(L_b+L_e) + \lambda_v(L_v)$$
where $\lambda_s$ and $\lambda_v$ are hyperparameters that dictate the desired strength of the regularization.


\section[Structure of conditional computation policies in deep models]{Structure of conditional computation\\ policies in deep models}

Conditional computation can be added to models through all kinds of parameterizations. There are a few criteria that must be met in order to ensure that applying conditional computation is useful:
\begin{itemize}
\item there should be a natural and hardware-friendly way to distribute computation;
  \item the number of computation subsets should be at least an order of magnitude smaller than the number of parameters;
 \item the overhead of computing the policy should be minimal;
 \item each computation subset should be ``capable'' of discriminating a subset of the input space.
\end{itemize}

We proposed and investigated the performance of two such models, in the context of training Deep Neural Networks.

\subsection{Fully connected architectures}
\label{section-fcarch}
A common observation in fully connected neural architectures is that their activation is often very sparse: i.e., most of the neurons tend to be zero, especially when using activation functions such as rectifying linear units.

Another observation, now empirically verified \citep{li2015convergent}, is that neurons tend to detect the presence of multiple patterns \emph{together}, and as such form natural relevant groups of computation (which we can assume are conditioned on input subspaces).

As such, a natural way of grouping computation in this architecture is, for a given layer, to divide neurons into groups, which we well refer to as blocks. For this division to be hardware friendly, we impose for all groups to have the same size.


With these groupings, a way of parameterizing the policy is to associate a Bernoulli unit to each group, and make this unit dependent on the activations of the previous layer. This is illustrated in Figure \ref{fig-fccondnet}.


\begin{figure}[!h]
  \centering
%\includegraphics[scale=0.6]{figures/}
  \input{figures/blockcondnet.tex}
  \caption[Fully-connected block conditional computation architecture]{A fully-connected block conditional computation architecture. In blue, the target model, divided in blocks of units. In red, the computation policy, calculated from the previous layer's activation.}
  \label{fig-fccondnet}
\end{figure}

The policy units themselves are computed as normal sigmoid units, with a different set of weights for each layer (each predicting the policy for all the layer's blocks):
$$\sigma^{(i)} = s(W^{\pi_i} h+b^{\pi_i})$$
where $s$ is the sigmoid function (see Section \ref{section-non-linearities}, here $\sigma$ instead denotes the policy probabilities).


\subsection{Convolutional architectures}
\label{section-convarch}


In the convolutional setting, there are many ways in which we could turn off parts of the target model, some more expensive than others. For example we could choose which channels we want to use at each layer, but that would require the policy to be complex and expensive.

A simple alternative, inspired by \cite{huang2016deep}, is to skip layers in a residual manner. If layer $i$ is skipped, then the input of layer $i+1$ is simply the activations of layer $i-1$ (which can itself be the result of a skip). In \cite{huang2016deep} layers are randomly skipped during training, in a similar fashion as dropout \citep{Hinton2012}.

In this case, we would like to use conditional computation to choose which layers to skip as a function of the input. As such the policy only has to have a single unit per layer. This is illustrated in Figure \ref{fig-convcond}.


\begin{figure}[!h]
  \centering
%\includegraphics[scale=0.6]{figures/}
  \input{figures/convcond.tex}
  \caption{A convolutional layer-skipping architecture}
  \label{fig-convcond}
\end{figure}


\section{Related Work}
\citet{Ba2013} proposed a learning algorithm called \textit{standout} for computing an
input-dependent dropout distribution at every node. As opposed to our layer-wise method, standout
computes a one-shot dropout mask over the entire network, conditioned on the input to the
network. Additionally, masks are unit-wise, while our approach is more general and can handle groups of units, for example it can use masks that span blocks of units.
\citet{bengio2013estimating} introduced Stochastic Times Smooth neurons as gaters for conditional
computation within a deep neural network. STS neurons are highly non-linear and non-differentiable
functions learned using estimators of the gradient obtained through REINFORCE. They allow a sparse
binary gater to be computed as a function of the input, thus reducing computations in the
then sparse activation of hidden layers.

\citet{Stollenga2014} recently proposed to learn a sequential decision process over the
filters of a convolutional neural network (CNN). As in our work, a direct policy search
method was chosen to find the parameters of a control policy. Their problem formulation
differs from ours mainly in the notion of decision ``stage''. In their model, an input is
first fed through a network, the activations are computed during forward propagation then
they are served to the next decision stage. The goal of the policy is to select relevant
filters from the previous stage to improve the decision accuracy on the current
example.  They also use a gradient-free evolutionary algorithm, in contrast to our gradient-based method.
%\citet{Schaul2011}

The Deep Sequential Neural Network (DSNN) model of \citet{Denoyer2014} is possibly closest
to our approach. The control process is carried over the layers of the network and uses
the output of the previous layer to compute actions. The REINFORCE algorithm is used to
train the policy with the reward/cost function being defined as the loss at the output in
the base network. DSNN considers the general problem of choosing between different
type of mappings (weights) in a composition of functions. However, they test their model
on datasets in which different modes are prominent, making it easy for a policy to
distinguish between them.

Another point of comparison for our work are the recent attention models \citep{Mnih2014b, gregor2015draw, xu2015show}. These models typically learn a policy, or a form of policy, that allows them to selectively attend to parts of their input \emph{sequentially}, in a visual 2D environment. Both attention and conditional computation share a goal of reducing computation times and maintaining accuracy. While attention aims to perform dense computations on subsets of the inputs, our conditional computation approach aims to be more general, since the policy can target subsets of computation throughout the network architecture. As such, it can lead to more distributed computation at various levels of feature resolution. It should be possible to combine these two approaches, since one acts on the input space and the other acts on the representation space, although the required policy space to do this may well be more complex, and thus harder to train.
