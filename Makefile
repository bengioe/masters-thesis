
F = masters-thesis
_chapters = $(shell ls chapters)
chapters = $(addprefix chapters/,$(_chapters))

default: all

loop:
	while inotifywait -e close_write $(F).tex $(chapters); do make all; done

loop_with_errors:
	while inotifywait -q -o /dev/null -e close_write $(F).tex $(chapters); do make -s see_errors; done

none:


all:
	pdflatex -interaction=nonstopmode $(F)
	bibtex $(F)
	pdflatex -interaction=nonstopmode $(F)
	pdflatex -interaction=nonstopmode $(F)

compile_line = pdflatex -interaction=nonstopmode -file-line-error
grep_cmd = | grep -A 2 -i ".*:[0-9]*:.*" || true

see_errors:
	@echo "make"
	$(compile_line) $(F) $(grep_cmd)
	bibtex $(F) | grep -i "warning" || true
	$(compile_line) $(F) $(grep_cmd)
	$(compile_line) $(F) $(grep_cmd)


some:
	pdflatex  $(F)
	bibtex $(F)
	pdflatex  $(F)
	pdflatex  $(F)

rmtemp:
	rm masters-thesis.aux masters-thesis.bbl masters-thesis.bcf masters-thesis.blg masters-thesis.idx masters-thesis.log masters-thesis.out masters-thesis.run.xml masters-thesis.toc
